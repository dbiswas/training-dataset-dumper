############################################
# Z', alt. Z', dijet, V+jets (high-pT extrapolation - Tom Dingley)
############################################

# nominal Z' sample, used for tracking and jet variations within the TDD
mc20_13TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e7954_s3681_r13144_p5981
mc23_13p6TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e8514_s4162_r14622_p5981
mc23_13p6TeV.800030.Py8EG_A14NNPDF23LO_flatpT_Zprime_Extended.deriv.DAOD_FTAG1.e8514_s4159_r14799_p5981

