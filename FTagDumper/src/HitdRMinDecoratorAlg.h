#ifndef HdRMin_DECORATOR_ALG_HH
#define HdRMin_DECORATOR_ALG_HH

#include "AthenaBaseComps/AthReentrantAlgorithm.h"

#include "GaudiKernel/ToolHandle.h"
#include "AthContainers/AuxElement.h"
#include "StoreGate/WriteDecorHandleKey.h"
#include "StoreGate/ReadDecorHandleKey.h"

#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "AthContainers/AuxElement.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/VertexContainer.h"


class HitdRMinDecoratorAlg :  public AthReentrantAlgorithm {
public:

  /** Constructors */
  HitdRMinDecoratorAlg(const std::string& name, ISvcLocator *pSvcLocator);

  /** Main routines */
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext&) const override ;


private:

  using TMVC = xAOD::TrackMeasurementValidationContainer;

  SG::ReadHandleKey<xAOD::TrackParticleContainer>   m_tracks{this,"TrackParticles", "InDetTrackParticles", "Track Particles"};
  SG::ReadHandleKey<xAOD::VertexContainer>   m_Vertex{this,"Vertex", "PrimaryVertices", "Vertex"};

  SG::ReadHandleKey<TMVC>   m_JetPixelCluster{this,"JetAssociatedPixelClusters", "JetAssociatedPixelClusters", "PixelClusters"};
  SG::ReadHandleKey<TMVC>   m_JetSCTCluster{this,"JetAssociatedSCTClusters", "JetAssociatedSCTClusters", "SCTClusters"};

  SG::WriteDecorHandleKey< TMVC > m_sctDr { this, "JetAssociatedSCTClustersdR_closestTrackHit", m_JetSCTCluster, "dR_closestTrackHit"};
  SG::WriteDecorHandleKey< TMVC > m_pixDr { this, "JetAssociatedPixelClustersdR_closestTrackHit", m_JetPixelCluster, "dR_closestTrackHit"};

};


#endif
